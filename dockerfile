FROM rust:1.59 as builder

# Caching dependencies into image
WORKDIR /usr/src
RUN cargo new --bin eupim
WORKDIR /usr/src/eupim
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml
RUN cargo build --release
RUN rm src/*.rs

# Real build
COPY ./src ./src
RUN rm ./target/release/deps/eupim*
RUN cargo build --release

# Final image
FROM debian:bullseye-slim

COPY --from=builder /usr/src/eupim/target/release/eupim /usr/local/bin/eupim
RUN mkdir /etc/eupim
COPY --from=builder /usr/src/eupim/src/settings.toml /etc/eupim/settings.toml

CMD ["eupim"]