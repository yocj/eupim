# Eupim

## Descpription

Minecraft reverse proxy

## Modification

Penser à créer un lien symbolique vers le fichier settings.toml depuis /etc/eupim (à créer)

## À Faire

- [ ] Erreurs personnalisées pour encoder
- [ ] Logging (Logs avec plus de description et horodatage)
- [ ] Optimiser code
- [ ] Documentation
- [ ] Démarrer serveur minecraft si nouvelle connexion
