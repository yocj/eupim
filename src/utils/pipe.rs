use std::io::{Read, Write};
use std::net::{Shutdown, TcpStream};

pub fn pipe(incoming: &mut TcpStream, outgoing: &mut TcpStream) -> Result<(), String> {
    loop {
        let mut buffer = [0; 1024];
        match incoming.read(&mut buffer) {
            Ok(n) => {
                if n == 0 {
                    outgoing.shutdown(Shutdown::Both).unwrap();
                    break;
                }

                if outgoing.write(&buffer[..n]).is_ok() {
                    outgoing.flush().unwrap();
                }
            }
            Err(e) => {
                return Err(format!("Problem at {}", e));
            }
        }
    }
    Ok(())
}
