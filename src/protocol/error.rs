use std::fmt;

#[derive(Debug)]
pub enum DecodeError {
    VarIntMaxLengthError,
    MaxStringSizeError,
    U16BigEndianEOFError,
    U128BigEndianEOFError
}

#[derive(Debug)]
pub enum EncodeError {
    MaxStringSizeError,
    U16BigEndianError,
    U128BigEndianError
}


impl fmt::Display for DecodeError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DecodeError::VarIntMaxLengthError => write!(f, "The max length of a VarInt is 5"),
            DecodeError::MaxStringSizeError => write!(f, "The max length of a string is 32767"),
            DecodeError::U16BigEndianEOFError => write!(f, "Bufffer unfullfilled"),
            DecodeError::U128BigEndianEOFError => write!(f, "Bufffer unfullfilled")
        }
    }
}