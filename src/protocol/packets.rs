use crate::server::connection::State;
use crate::Encoder;
use std::io::Cursor;
use std::sync::Arc;

pub trait PacketHandler {
    fn as_bytes(&self) -> Vec<u8>;
    fn get_bound(&self) -> &PacketBound;
}

pub enum PacketBound {
    Client,
    Server,
}

pub struct Handshake {
    size: i32,
    id: i32,
    protocol_version: i32,
    address: String,
    port: u16,
    next_state: Arc<State>,
    bound: PacketBound,
}

impl Handshake {
    pub fn new(
        size: i32,
        id: i32,
        protocol_version: i32,
        address: String,
        port: u16,
        next_state: Arc<State>,
    ) -> Handshake {
        Handshake {
            size,
            id,
            protocol_version,
            address,
            port,
            next_state,
            bound: PacketBound::Server,
        }
    }
    pub fn get_address(&self) -> &String {
        &self.address
    }
}

impl PacketHandler for Handshake {
    fn as_bytes(&self) -> Vec<u8> {
        let mut buf = vec![0u8; self.size as usize + 1];
        let mut wrt = Cursor::new(&mut buf[..]);
        wrt.write_var_int(self.size).unwrap();
        wrt.write_var_int(self.id).unwrap();
        wrt.write_var_int(self.protocol_version).unwrap();
        wrt.write_string(self.address.to_string()).unwrap();
        wrt.write_u16_big_endian(self.port).unwrap();
        wrt.write_var_int(self.next_state.as_ref().get_state_value()).unwrap();

        buf
    }

    fn get_bound(&self) -> &PacketBound {
        &self.bound
    }
}

pub struct LoginStart {
    size: i32,
    id: i32,
    playername: String,
    has_uuid: Option<bool>,
    uuid: Option<u128>,
    bound: PacketBound,
    protocol_version: i32,
}

impl LoginStart {
    pub fn new(size: i32, id: i32, playername: String, has_uuid: Option<bool>, uuid: Option<u128>, protocol_version: i32) -> LoginStart {
        LoginStart {
            size,
            id,
            playername,
            has_uuid,
            uuid,
            bound: PacketBound::Server,
            protocol_version,
        }
    }
}

impl PacketHandler for LoginStart {
    fn as_bytes(&self) -> Vec<u8> {
        let packet_size = self.size;
        let mut buf = vec![0u8; packet_size as usize + 1];
        let mut wrt = Cursor::new(&mut buf[..]);
        wrt.write_var_int(packet_size).unwrap();
        wrt.write_var_int(self.id).unwrap();
        wrt.write_string(self.playername.to_string()).unwrap();
        if self.protocol_version >= 761 {
            wrt.write_bool(self.has_uuid.unwrap());
            wrt.write_u128_big_endian(self.uuid.unwrap()).unwrap();
        }

        buf
    }
    fn get_bound(&self) -> &PacketBound {
        &self.bound
    }
}

pub struct Disconnect {
    size: i32,
    id: i32,
    reason: String,
    bound: PacketBound,
}

impl Disconnect {
    pub fn new(reason: String) -> Disconnect {
        let reason = format!("{{\"text\": \"{}\", \"color\": \"red\"}}", reason);
        let size: i32 = 1 + reason.len() as i32 + 1;
        Disconnect {
            size,
            id: 0,
            reason,
            bound: PacketBound::Client,
        }
    }
}

impl PacketHandler for Disconnect {
    fn as_bytes(&self) -> Vec<u8> {
        let mut buf = vec![0; self.size as usize + 1];
        let mut wrt = Cursor::new(&mut buf[..]);
        wrt.write_var_int(self.size).unwrap();
        wrt.write_var_int(self.id).unwrap();
        wrt.write_string(self.reason.clone()).unwrap();

        buf
    }

    fn get_bound(&self) -> &PacketBound {
        &self.bound
    }
}

pub struct Status {
    size: i32,
    id: i32,
    bound: PacketBound,
}

impl Status {
    pub fn new(size: i32, id: i32) -> Status {
        Status {
            size,
            id,
            bound: PacketBound::Server,
        }
    }
}

impl PacketHandler for Status {
    fn as_bytes(&self) -> Vec<u8> {
        let mut buf = vec![0; self.size as usize + 1];
        let mut wrt = Cursor::new(&mut buf[..]);
        wrt.write_var_int(self.size).unwrap();
        wrt.write_var_int(self.id).unwrap();
        buf
    }

    fn get_bound(&self) -> &PacketBound {
        &self.bound
    }
}
