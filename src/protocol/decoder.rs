use std::io::Read;
use byteorder::{BigEndian, ReadBytesExt};
use crate::protocol::error;

pub trait Decoder {
    fn read_var_int(&mut self) -> Result<i32, error::DecodeError>;
    fn read_string(&mut self) -> Result<String, error::DecodeError>;
    fn read_u16_big_endian(&mut self) -> Result<u16, error::DecodeError>;
    fn read_bool(&mut self) -> bool;
    fn read_u128_big_endian(&mut self) -> Result<u128, error::DecodeError>;
}

impl<R: Read> Decoder for R {
    fn read_var_int(&mut self) -> Result<i32, error::DecodeError> {
        let mut bytes = 0;
        let mut output = 0;
    
        loop {
            let byte = self.read_u8().unwrap();
            let value = (byte & 0b01111111) as i32;
    
            output |= value << 7 * bytes;
            bytes += 1;
    
            if bytes > 5 {
                return Err(error::DecodeError::VarIntMaxLengthError);
            }
    
            if (byte & 0b10000000) == 0 {
                break;
            }
        }
        Ok(output)
    }

    fn read_string(&mut self) -> Result<String, error::DecodeError> {
        let string_len = self.read_var_int().unwrap();

        if string_len > 32767 {
            return Err(error::DecodeError::MaxStringSizeError);
        }

        let mut buf = vec![0; string_len as usize];
        self.read_exact(&mut buf).unwrap();

        Ok(String::from_utf8(buf).unwrap())
    }

    fn read_u16_big_endian(&mut self) -> Result<u16, error::DecodeError> {
        match self.read_u16::<BigEndian>() {
            Ok(n) => return Ok(n),
            Err(_) => return Err(error::DecodeError::U16BigEndianEOFError)
        }
    }

    fn read_bool(&mut self) -> bool {
        self.read_u8().unwrap() != 0
    }

    fn read_u128_big_endian(&mut self) -> Result<u128, error::DecodeError> {
        match self.read_u128::<BigEndian>() {
            Ok(n) => return Ok(n),
            Err(_) => return Err(error::DecodeError::U128BigEndianEOFError)
        }
    }
}