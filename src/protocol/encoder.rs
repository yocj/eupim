use std::io::Write;
use byteorder::{BigEndian, WriteBytesExt};
use crate::protocol::error;

pub trait Encoder {
    fn write_var_int(&mut self, value: i32) -> Result<(), error::EncodeError>;
    fn write_string(&mut self, value: String) -> Result<(), error::EncodeError>;
    fn write_u16_big_endian(&mut self, value: u16) -> Result<(), error::EncodeError>;
    fn write_bool(&mut self, value: bool) -> u8;
    fn write_u128_big_endian(&mut self, value: u128) -> Result<(), error::EncodeError>;
}

impl<W: Write> Encoder for W {
    fn write_var_int(&mut self, mut value: i32) -> Result<(), error::EncodeError> {
        loop {
            let byte = (value & 0b01111111) as u8;
            value = value >> 7;

            if value == 0 {
                self.write_u8(byte).unwrap();
                break;
            }

            self.write_u8(byte | 0b10000000).unwrap();
        }
        Ok(())
    }

    fn write_string(&mut self, value: String) -> Result<(), error::EncodeError> {
        let string_len = value.len();

        if string_len > 32767 {
            return Err(error::EncodeError::MaxStringSizeError);
        }

        self.write_var_int(string_len as i32)?;
        self.write_all(value.as_bytes()).unwrap();
        Ok(())
    }

    fn write_u16_big_endian(&mut self, value: u16) -> Result<(), error::EncodeError> {
        match self.write_u16::<BigEndian>(value) {
            Ok(n) => return Ok(n),
            Err(_) => return Err(error::EncodeError::U16BigEndianError)
        }
    }

    fn write_bool(&mut self, value: bool) -> u8 {
        if value {
            return 1;
        } else {
            return 0;
        }
    }

    fn write_u128_big_endian(&mut self, value: u128) -> Result<(), error::EncodeError> {
        match self.write_u128::<BigEndian>(value) {
            Ok(n) => return Ok(n),
            Err(_) => return Err(error::EncodeError::U128BigEndianError)
        }
    }
}