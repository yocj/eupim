use std::collections::HashMap;
use std::net::TcpListener;
use super::connection::Connection;
use std::thread;
use std::sync::Arc;

pub struct Server {
    bind_address: String,
}

impl Server {
    pub fn new(bind_address: String) -> Server {
        Server {bind_address}
    }

    pub fn listen(&self, servers: Arc<HashMap<String, String>>) -> Result<(), Box<dyn std::error::Error>> {
        let listener = TcpListener::bind(&self.bind_address)?;
        println!("Server started, waiting for connections...");
        for stream in listener.incoming() {
            println!("Incoming connection");
            let servers = Arc::clone(&servers);
            thread::spawn(move || Connection::new(stream.unwrap(), servers).handle_connection());
        }
        Ok(())
    }
}