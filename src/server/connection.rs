use std::collections::HashMap;
use std::io::prelude::*;
use std::io::Cursor;
use std::net::{Shutdown, TcpStream};
use std::sync::Arc;
use std::thread;

use crate::packets::{Disconnect, Handshake, LoginStart, PacketBound, PacketHandler, Status};
use crate::pipe;
use crate::Decoder;

pub enum State {
    Handshaking,
    Status,
    Login,
}

impl State {
    pub fn get_state_value(&self) -> i32 {
        match self {
            State::Handshaking => 0,
            State::Status => 1,
            State::Login => 2
        }
    }
}

pub struct Connection {
    state: Arc<State>,
    servers: Arc<HashMap<String, String>>,
    player_connection: TcpStream,
    server_connection: Option<TcpStream>,
    protocol_version: Option<i32>,
}

impl Connection {
    pub fn new(player_connection: TcpStream, servers: Arc<HashMap<String, String>>) -> Connection {
        Connection {
            state: Arc::new(State::Handshaking),
            servers,
            player_connection,
            server_connection: None,
            protocol_version: None,
        }
    }

    fn get_target_address(&self, address: String) -> Result<String, std::io::Error> {
        let target_address = self.servers.get(&address[..]);
        if target_address.is_some() {
            Ok(target_address.unwrap().to_string())
        } else {
            Err(std::io::Error::from(std::io::ErrorKind::AddrNotAvailable))
        }
    }

    fn initiate_connection(&mut self, target_address: String) -> Result<(), std::io::Error> {
        let stream = TcpStream::connect(target_address)?;
        self.server_connection = Some(stream.try_clone().unwrap());
        Ok(())
    }

    fn send_packet(&mut self, packet: &dyn PacketHandler) {
        let buf = packet.as_bytes();
        match packet.get_bound() {
            PacketBound::Client => {
                self.player_connection.write(&buf[..]).unwrap();
            }
            PacketBound::Server => {
                self.server_connection
                    .as_ref()
                    .unwrap()
                    .write(&buf[..])
                    .unwrap();
            }
        }
    }

    fn pipe_connections(&mut self) {
        let mut player = self.player_connection.try_clone().unwrap();
        let mut player2 = self.player_connection.try_clone().unwrap();

        let mut server = self
            .server_connection
            .as_ref()
            .unwrap()
            .try_clone()
            .unwrap();
        let mut server2 = self
            .server_connection
            .as_ref()
            .unwrap()
            .try_clone()
            .unwrap();

        let forward = thread::spawn(move || pipe(&mut player, &mut server));
        let backward = thread::spawn(move || pipe(&mut server2, &mut player2));

        match forward.join() {
            Ok(_) => (),
            Err(_) => {
                eprintln!("Shutdowning client connection");
                self.player_connection.shutdown(Shutdown::Both).unwrap();
            }
        }
        match backward.join() {
            Ok(_) => (),
            Err(_) => {
                eprintln!("Shutdowning server connection");
                self.server_connection
                    .as_ref()
                    .unwrap()
                    .shutdown(Shutdown::Both)
                    .unwrap();
            }
        }
    }

    pub fn handle_connection(&mut self) {
        loop {
            let mut buf = [0; 1024];
            match self.player_connection.read(&mut buf) {
                Ok(n) => {
                    if n == 0 {
                        break;
                    } else {
                        let mut rdr = Cursor::new(buf);
                        match self.state.as_ref() {
                            State::Handshaking => {
                                if let Ok(size) = rdr.read_var_int() {
                                    let id = rdr.read_var_int().unwrap();
                                    let protocol_version = rdr.read_var_int().unwrap();
                                    self.protocol_version = Some(protocol_version);
                                    let address = rdr.read_string().unwrap();
                                    let port = rdr.read_u16_big_endian().unwrap();
                                    let next_state = rdr.read_var_int().unwrap();
                                    let next_state = if next_state == 1 {
                                        Arc::new(State::Status)
                                    } else {
                                        Arc::new(State::Login)
                                    };
                                    self.state = Arc::clone(&next_state);
                                    let handshake_packet = Handshake::new(
                                        size,
                                        id,
                                        protocol_version,
                                        address,
                                        port,
                                        next_state,
                                    );
                                    if let Ok(target_address) = self.get_target_address(
                                        handshake_packet.get_address().to_string(),
                                    ) {
                                        if self.initiate_connection(target_address).is_ok() {
                                            self.send_packet(&handshake_packet);
                                        } else {
                                            let error = "Server not started";
                                            eprintln!("{}", error);
                                            let error = Disconnect::new(error.to_string());
                                            self.send_packet(&error);
                                            break;
                                        }
                                    } else {
                                        let error = "Couldn't find server in settings";
                                        eprintln!("{}", error);
                                        let error = Disconnect::new(error.to_string());
                                        self.send_packet(&error);
                                        break;
                                    }
                                }
                            }
                            State::Login => {
                                if let Ok(size) = rdr.read_var_int() {
                                    let id = rdr.read_var_int().unwrap();
                                    let playername = rdr.read_string().unwrap();
                                    let protocol_version = self.protocol_version.unwrap();
                                    let login_packet = if protocol_version < 761 {
                                        LoginStart::new(size, id, playername, None, None, protocol_version)
                                    } else {
                                        let has_uuid = Some(rdr.read_bool());
                                        let uuid = Some(rdr.read_u128_big_endian().unwrap());
                                        LoginStart::new(size, id, playername, has_uuid, uuid, protocol_version)
                                    };
                                    self.send_packet(&login_packet);
                                    self.pipe_connections();
                                }
                            }
                            State::Status => {
                                if let Ok(size) = rdr.read_var_int() {
                                    let id = rdr.read_var_int().unwrap();
                                    let status_packet = Status::new(size, id);
                                    self.send_packet(&status_packet);
                                    self.pipe_connections();
                                }
                            }
                        }
                    }
                }
                Err(e) => {
                    println!("Error: {}", e);
                }
            }
        }
    }
}
