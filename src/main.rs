use config::Config;
use std::collections::HashMap;
use std::sync::Arc;

mod utils;
mod server;
mod protocol;

use server::proxy::Server;
use protocol::packets;
use protocol::decoder::Decoder;
use protocol::encoder::Encoder;
use utils::pipe::pipe;

fn main() {
    let settings = Config::builder()
                        .add_source(config::File::with_name("/etc/eupim/settings.toml")).build().unwrap();
    let settings = settings.try_deserialize::<HashMap<String, HashMap<String, String>>>().unwrap();
    let bind_address = settings.get("ProxyServer").unwrap().get("bind_address").unwrap().to_string();
    let servers = Arc::new(settings.get("Servers").unwrap().clone());
    Server::new(bind_address).listen(servers).unwrap();
}